package com.lee.vo;

import com.fasterxml.jackson.annotation.JsonRawValue;
import com.lee.properties.StatusConstants;
import lombok.Getter;

/**
 * @author Lee
 * @date 2018年10月10日 上午10:54:06
 */
@Getter
public class ResponseVO {

    // 状态码
    private int code;
    // 是否成功
    private boolean success;
    // 返回信息
    private String message;
    // 返回实体结果
    private Object data;

    private static final String successMessage = "请求成功";

    public static ResponseVO success() {

        return new ResponseVO(StatusConstants.OK, true, successMessage, null);
    }

    public static ResponseVO success(Object data) {
        return data instanceof JsonString ? new JsonResponseVO(StatusConstants.OK, true, successMessage, (JsonString) data) :
                new ResponseVO(StatusConstants.OK, true, successMessage, data);
    }

    public static ResponseVO failure(int code) {
        return new ResponseVO(code, false, "处理失败", null);
    }

    public static ResponseVO failure(int code, String message) {
        return new ResponseVO(code, false, message, null);
    }

    private ResponseVO(int code, boolean success, String message, Object data) {
        this.code = code;
        this.success = success;
        this.message = message;
        this.data = data;
    }

    @Getter
    private static class JsonResponseVO extends ResponseVO {

        // 已经被json序列化的字符串
        @JsonRawValue
        private String data;

        private JsonResponseVO(int code, boolean success, String message, JsonString json) {
            super(code, success, message, null);
            this.data = json.getValue();
        }

    }

}
