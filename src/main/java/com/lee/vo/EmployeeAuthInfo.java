package com.lee.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * @author Lee HN
 * @date 2019/6/21 14:58
 * 员工权限认证信息
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class EmployeeAuthInfo implements UserDetails {

    private Long id;
    // 账号
    private String username;
    @JsonIgnore
    private String password;
    //员工状态
    private Integer status;
    //部门所属
    private String deptPath;

    @JsonIgnore
    private List<GrantedAuthority> authorityList;

    public static EmployeeAuthInfo of(long id) {
        return EmployeeAuthInfo.of(id, null, null, null, null, null);
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return authorityList;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return Objects.equals(status, 1);
    }
}
