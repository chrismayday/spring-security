package com.lee.vo;

import lombok.AllArgsConstructor;

/**
 * @author Lee HN
 * @date 2019/6/27 11:04
 */
@AllArgsConstructor
public class JsonString {

    private String value;

    public static JsonString of(Object value) {
        return new JsonString(String.valueOf(value));
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof JsonString)) return false;
        final JsonString other = (JsonString) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$value = this.getValue();
        final Object other$value = other.getValue();
        if (this$value == null ? other$value != null : !this$value.equals(other$value)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof JsonString;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $value = this.getValue();
        result = result * PRIME + ($value == null ? 43 : $value.hashCode());
        return result;
    }

    public String toString() {
        return "JsonString(value=" + this.getValue() + ")";
    }
}
