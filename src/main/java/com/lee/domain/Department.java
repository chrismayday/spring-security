package com.lee.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Lee HN
 * @date 2019/7/1 14:17
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class Department implements Serializable {

    private static final long serialVersionUID = -7319402222525117325L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    //父级id
    private Long parentId;
    // 深度
    private Integer level;
    // 根路径,用,分隔
    private String rootPath;
    // 排序字段
    private Integer seq;
    // 部门名称
    private String name;
    // 备注
    private String remark;

    @OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY, mappedBy = "department")
    private List<Employee> employeeList;

}
