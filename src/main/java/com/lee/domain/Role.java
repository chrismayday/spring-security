package com.lee.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.List;

/**
 * @author Lee HN
 * @date 2019/6/21 14:29
 * 员工角色
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class Role implements GrantedAuthority {

    private static final long serialVersionUID = -6947149840890908394L;

    public static final String ROLE_PREFIX = "ROLE_";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @JsonIgnore
    private String name; // 角色名
    @JsonIgnore
    private Long addTime; // 添加时间
    @JsonIgnore
    private String remark; // 备注信息

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    @JoinTable(name = "role_authority", joinColumns = {@JoinColumn(name = "role_id")},
            inverseJoinColumns = {@JoinColumn(name = "authority")})
    private List<Authority> authorityList;

    @Override
    public String getAuthority() {
        return ROLE_PREFIX + name;
    }
}
