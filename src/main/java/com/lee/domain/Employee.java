package com.lee.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * @author Lee HN
 * @date 2019/6/18 13:48
 * 员工信息表
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Employee implements Serializable {

    private static final long serialVersionUID = -6135478492945828203L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    // 账号
    private String username;
    // 用户密码
    private String password;
    // 昵称
    private String nickname;
    // 真名
    private String realname;
    //用户头像
    private String icon;
    // 性别 女0 男1
    private Integer sex;
    // 年龄
    private Integer age;
    // 手机号
    private String mobile;
    // 电子邮件
    private String email;
    // 员工状态
    private Integer status;
    // 添加时间
    private Long addTime;
    //最后更新时间
    private Long lastUpdateTime;

    @ManyToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinTable(name = "emp_role", joinColumns = {@JoinColumn(name = "emp_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private List<Role> roleList;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id")
    private Department department;

}
