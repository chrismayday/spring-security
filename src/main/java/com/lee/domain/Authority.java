package com.lee.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Lee HN
 * @date 2019/6/21 14:36
 * 角色权限
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class Authority implements GrantedAuthority {

    private static final long serialVersionUID = -6473179537599857382L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @JsonIgnore
    private String name;
    @JsonIgnore
    private Long addTime;
    @JsonIgnore
    private String remark;

    @Override
    public String getAuthority() {
        return name;
    }
}
