package com.lee.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;

/**
 * @author 向帅
 * @Date 2019/7/7 18:34
 **/
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class EmployeeSocial implements Serializable{

    private static final long serialVersionUID = -346093700259033592L;

    @EmbeddedId
    private SocialKey socialKey;
    //第三方头像
    private String iconUrl;

    private Long addTime;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor(staticName = "of")
    @Embeddable
    public static class SocialKey implements Serializable {

        private static final long serialVersionUID = -155966170784600460L;

        private Long employeeId;

        private String openId;

        private String providerId;
    }
}
