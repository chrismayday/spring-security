package com.lee.security.handler;

import com.google.common.collect.Lists;
import com.lee.vo.ResponseVO;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.refresh.RefreshTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author lee
 * @date 2018年12月20日
 */
@RestController
@RequestMapping("/oauth2")
public class OAuth2Controller {

    private BasicTokenCheck basicTokenCheck;

    private ClientDetailsService clientDetailsService;

    private TokenGranter tokenGranter;

    private static final String refresh_token_type = "refresh_token";

    private static final List<String> scopeList = Lists.newArrayList("all");

    public OAuth2Controller(BasicTokenCheck basicTokenCheck, ClientDetailsService clientDetailsService, AuthorizationServerTokenServices defaultAuthorizationServerTokenServices) {
        this.basicTokenCheck = basicTokenCheck;
        this.clientDetailsService = clientDetailsService;
        this.tokenGranter = new RefreshTokenGranter(defaultAuthorizationServerTokenServices, clientDetailsService, null);
    }

    @PostMapping("/token/refresh")
    public ResponseVO refreshToken(@RequestHeader("Authorization") String header,
                                   @RequestParam Map<String, String> parameters) {

        if (parameters.get(refresh_token_type) == null)
            throw new IllegalArgumentException("refresh_token不能为空");

        ClientDetails clientDetails = basicTokenCheck.obtainClientDetails(header);

        TokenRequest tokenRequest = new TokenRequest(parameters, clientDetails.getClientId(), scopeList, refresh_token_type);

        OAuth2AccessToken token = tokenGranter.grant(tokenRequest.getGrantType(), tokenRequest);

        return ResponseVO.success(token);
    }

}
