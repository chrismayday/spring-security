package com.lee.security.properties;

import lombok.Data;

/**
 * @author lee
 * @date 2018/11/9 16:06
 */
@Data
public class ValidateCodeProperties {

	private SmsCodeProperties smsCode = new SmsCodeProperties();

	private ImageCodeProperties imageCode = new ImageCodeProperties();

}
