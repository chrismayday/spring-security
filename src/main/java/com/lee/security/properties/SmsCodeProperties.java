package com.lee.security.properties;

import lombok.Data;

/**
 * @author lee
 * @date 2018/11/9 16:07
 */
@Data
public class SmsCodeProperties {

    private Integer  length = 4;

    private Integer expireIn = 300;

    private String url;


}
