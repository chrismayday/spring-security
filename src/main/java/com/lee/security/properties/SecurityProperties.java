package com.lee.security.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author lee
 * @date 2018/11/9 14:48
 */
@Data
@ConfigurationProperties(prefix = "security")
public class SecurityProperties {

    private ValidateCodeProperties validateCode = new ValidateCodeProperties();

    private Oauth2Properties oauth2 = new Oauth2Properties();

}
