package com.lee.security.config;

import com.lee.security.provider.SecurityConfigProviderManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;

/**
 * @author lee
 * @date 2018/11/8 14:46
 */
@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class MyResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Autowired
    private AuthenticationEntryPoint myAuthenticationEntryPoint;

    @Autowired
    private AccessDeniedHandler myAccessDeniedHandler;

    @Autowired
    private SecurityConfigProviderManager securityConfigProviderManager;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        securityConfigProviderManager.config(http);
        http.csrf().requireCsrfProtectionMatcher(request -> request.getServletPath().contains("/druid") ? false : true)
                .disable();
        http.authorizeRequests().anyRequest().permitAll();
    }

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.authenticationEntryPoint(myAuthenticationEntryPoint).accessDeniedHandler(myAccessDeniedHandler);
    }

}
