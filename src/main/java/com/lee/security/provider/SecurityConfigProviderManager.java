package com.lee.security.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 安全配置管理器
 * 
 * @author lee
 * @date 2018年10月31日
 */
@Component
public class SecurityConfigProviderManager {

	@Autowired
	private List<SecurityConfigProvider> securityConfigProviderList;

	// 装配所有配置提供者
	public void config(HttpSecurity http) {
		securityConfigProviderList.forEach(provider -> provider.configure(http));
	}

}
