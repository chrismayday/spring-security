package com.lee.security.provider;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;

/**
 * @author lee
 * @date 2018年10月31日
 */
public interface SecurityConfigProvider {

    void configure(HttpSecurity builder);

}
