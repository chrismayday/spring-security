package com.lee.security.provider.social;

import java.util.concurrent.Future;

/**
 * 验证第三方登陆请求是否有效
 * 
 * @author lee
 * @date 2018年11月20日
 */
public interface SocialTokenCheck {

	/**
	 * 验证openID和token请求返回的openId是否匹配
	 * 
	 * @param openId
	 * @param socialToken
	 */
	Future<Boolean> tokenCheck(String openId, String socialToken);

}
