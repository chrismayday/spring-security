package com.lee.security.provider.social;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.Future;

/**
 * @author lee
 * @date 2018年11月21日
 */
@Component
public class SocialTokenCheckManager {

    @Autowired
    private Map<String, SocialTokenCheck> socialTokenCheckMap;

    private static final String beanSuffix = "TokenCheck";

    public Future<Boolean> tokenCheck(String openId, String socialToken, String providerId) {
        SocialTokenCheck socialTokenCheck = socialTokenCheckMap.get(providerId + beanSuffix);
        if (socialTokenCheck != null)
            return socialTokenCheck.tokenCheck(openId, socialToken);
        throw new AuthenticationServiceException("不支持的第三方社交平台" + providerId);
    }

}
