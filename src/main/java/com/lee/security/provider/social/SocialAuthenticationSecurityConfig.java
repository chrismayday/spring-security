package com.lee.security.provider.social;

import com.lee.security.provider.SecurityConfigProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;

/**
 * 将配置的过滤器放入
 *
 * @author Lee
 * @date 2018年6月1日
 */
@Component
public class SocialAuthenticationSecurityConfig implements SecurityConfigProvider {

    @Autowired
    private AuthenticationSuccessHandler myAuthenticationSuccessHandler;

    @Autowired
    private AuthenticationFailureHandler myAuthenticationFailureHandler;

    @Autowired
    private AuthenticationManager providerManager;

    @Override
    public void configure(HttpSecurity http) {

        SocialAuthenticationFilter socialAuthenticationFilter = new SocialAuthenticationFilter();
        socialAuthenticationFilter.setAuthenticationManager(providerManager);
        socialAuthenticationFilter.setAuthenticationSuccessHandler(myAuthenticationSuccessHandler);
        socialAuthenticationFilter.setAuthenticationFailureHandler(myAuthenticationFailureHandler);

        http.addFilterAfter(socialAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);// http请求时加入过滤器

    }

}
