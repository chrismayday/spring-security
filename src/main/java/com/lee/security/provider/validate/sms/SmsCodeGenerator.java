/**
 *
 */
package com.lee.security.provider.validate.sms;

import com.lee.security.properties.SecurityProperties;
import com.lee.security.provider.validate.ValidateCode;
import com.lee.security.provider.validate.ValidateCodeGenerator;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * 短信验证码生成器
 *
 * @author Lee
 * @date 2018年6月1日
 */
@Component
public class SmsCodeGenerator implements ValidateCodeGenerator {

    @Autowired
    private SecurityProperties securityProperties;

    @Override
    public ValidateCode generate(ServletWebRequest request) {
        String code = RandomStringUtils.randomNumeric(securityProperties.getValidateCode().getSmsCode().getLength());
        return ValidateCode.of(code, securityProperties.getValidateCode().getSmsCode().getExpireIn());
    }

}
