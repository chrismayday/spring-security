package com.lee.security.provider.validate.image;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lee.security.provider.validate.ValidateCode;

import java.awt.image.BufferedImage;

/**
 * 图片验证码
 *
 * @author Lee
 * @date 2018年6月1日
 */
public class ImageCode extends ValidateCode {

    private static final long serialVersionUID = -6020470039852318468L;

    @JsonIgnore
    private transient BufferedImage image;

    public static ImageCode of(BufferedImage image, String code, int expireIn) {
        return new ImageCode(image, code, expireIn);
    }

    private ImageCode(BufferedImage image, String code, Integer expireIn) {
        super(code, expireIn);
        this.image = image;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

}
