/**
 * 
 */
package com.lee.security.provider.validate;

import org.springframework.web.context.request.ServletWebRequest;

/**
 * 校验码生成器
 * 
 * @author Lee
 * @date 2018年10月1日
 */
public interface ValidateCodeGenerator {

	/**
	 * 生成校验码
	 * @param request
	 * @return
	 */
	ValidateCode generate(ServletWebRequest request);
	
}
