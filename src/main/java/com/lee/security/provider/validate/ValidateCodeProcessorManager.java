/**
 * 
 */
package com.lee.security.provider.validate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 校验码处理器管理器
 * 
 * @author Lee
 * @date 2018年6月1日
 */
@Component
public class ValidateCodeProcessorManager {

	private final String beanSuffix = "CodeProcessor";

	@Autowired
	private Map<String, ValidateCodeProcessor> validateCodeProcessorMap;

	/**
	 * @param type
	 * @return
	 */
	public ValidateCodeProcessor findValidateCodeProcessor(ValidateCodeType type) {
		String name = type.toString() + beanSuffix;
		ValidateCodeProcessor processor = validateCodeProcessorMap.get(name);
		if (processor == null)
			throw new AuthenticationServiceException("验证码处理器" + name + "不存在");
		return processor;
	}

}
