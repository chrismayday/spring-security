/**
 *
 */
package com.lee.security.provider.validate;

import com.lee.security.properties.SecurityConstants;

/**
 *
 * 校验码类型
 *
 * @author Lee
 * @date 2018年6月1日
 */
public enum ValidateCodeType {

    /**
     * 短信验证码
     */
    sms {
        @Override
        public String getParamName() {
            return SecurityConstants.DEFAULT_PARAMETER_NAME_CODE_SMS;
        }
    },
    /**
     * 图片验证码
     */
    image {
        @Override
        public String getParamName() {
            return SecurityConstants.DEFAULT_PARAMETER_NAME_CODE_IMAGE;
        }
    };

    /**
     * 校验时从请求中获取的参数的名字
     *
     * @return
     */
    public abstract String getParamName();

}
