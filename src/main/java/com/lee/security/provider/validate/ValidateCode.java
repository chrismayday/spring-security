/**
 *
 */
package com.lee.security.provider.validate;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

/**
 * 验证码信息封装类
 *
 * @author Lee
 * @date 2018年6月1日
 */
public class ValidateCode implements Serializable {

    private static final long serialVersionUID = -8171878100877507008L;

    private String code;

    private long expireTime;

    public static ValidateCode of(String code, Integer expireIn) {
        return new ValidateCode(code, expireIn);
    }

    public ValidateCode() {
    }

    protected ValidateCode(String code, int expireIn) {
        this.code = code;
        this.expireTime = System.currentTimeMillis() + expireIn * 1000;
    }

    public ValidateCode(String code, long expireTime) {
        this.code = code;
        this.expireTime = expireTime;
    }

    @JsonIgnore
    public boolean isExpired() {
        return System.currentTimeMillis() > expireTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(long expireTime) {
        this.expireTime = expireTime;
    }

}
