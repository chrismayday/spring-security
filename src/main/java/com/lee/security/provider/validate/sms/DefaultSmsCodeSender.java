/**
 * 
 */
package com.lee.security.provider.validate.sms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 默认的短信验证码发送器
 * 
 * @author Lee
 * @date 2018年6月1日
 */
public class DefaultSmsCodeSender implements SmsCodeSender {

	private static final Logger log = LoggerFactory.getLogger(DefaultSmsCodeSender.class);

	@Override
	public void sendCode(String mobile, String smsScene, String code) {
		log.warn("请配置真实的短信验证码发送器(SmsCodeSender)");
		log.info("向手机" + mobile + "发送短信验证码" + code + "发送场景为" + smsScene);
	}

}
