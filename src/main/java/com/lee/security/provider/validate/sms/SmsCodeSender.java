/**
 *
 */
package com.lee.security.provider.validate.sms;

/**
 *
 * @author Lee
 * @date 2018年6月1日
 */
public interface SmsCodeSender {

    /**
     *
     * @param mobile
     * @param smsScene
     * @param code
     */
    void sendCode(String mobile, String smsScene, String code);

}
