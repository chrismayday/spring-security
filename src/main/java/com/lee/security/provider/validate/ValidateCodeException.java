package com.lee.security.provider.validate;

import org.springframework.security.core.AuthenticationException;

/**
 * @author Lee
 * @date 2018年10月1日
 */
public class ValidateCodeException extends AuthenticationException {

	private static final long serialVersionUID = -7285211528095468156L;

	public ValidateCodeException(String msg) {
		super(msg);
	}

}
