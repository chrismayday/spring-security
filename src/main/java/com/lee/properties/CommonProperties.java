package com.lee.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lee
 * @date 2018/11/8 16:15
 */
@Data
@ConfigurationProperties("common")
public class CommonProperties {

    private Map<String, SmsTemplate> smsScene = new HashMap<>();

    private Map<String, Boolean> registeredScene = new HashMap<>();

}
