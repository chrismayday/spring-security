package com.lee.properties;

public interface StatusConstants {

    int OK = 0;// 请求成功

    int FAIL = -1;// 失败

    int NOT_AUTHENTICATED = -10000; // 未认证

    int BAD_CREDENTIAL = -10001; // 认证错误

    int CREDENTIAL_INVALID = -10002; // 证书已失效

    int NO_AUTHORITY = -10003; // 无权限

    int BAD_OPERATION = -10004; // 非法操作

    int SERVER_ERROR = -10005; // 服务器错误

    int CREDENTIAL_EXPIRED = -10006; // 证书已过期

    int BAD_STATUS = 10001; // 错误的状态

    int BAD_DATA_TYPE = 10002; // 错误的数据类型

    int NO_DATA = 10003; // 没有相关资源

}
