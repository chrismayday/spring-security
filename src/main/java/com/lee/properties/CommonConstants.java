package com.lee.properties;

import java.time.ZoneOffset;

public interface CommonConstants {

	/**
	 * 默认手机正则表达式
	 */
	final String DEFAULT_MOBILE_REGEX = "^0?1\\d{10}$";

}
