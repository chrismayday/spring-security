package com.lee.repository;

import com.lee.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

/**
 * @author 向帅
 * @Date 2019/7/7 18:36
 **/
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    @Query("select e from Employee e left join fetch e.roleList left join fetch e.department where e.username = :username")
    Optional<Employee> findByUsernameForAuth(String username);

    boolean existsByUsername(String mobile);
}
