package com.lee.repository;

import com.lee.domain.Department;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @author 向帅
 * @Date 2019/7/7 19:02
 **/
public interface DepartmentRepository extends JpaRepository<Department, Long> {

    Optional<Department> findByName(String deptName);

}
