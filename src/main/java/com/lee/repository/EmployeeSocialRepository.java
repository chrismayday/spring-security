package com.lee.repository;

import com.lee.domain.EmployeeSocial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author 向帅
 * @Date 2019/7/7 18:36
 **/
public interface EmployeeSocialRepository extends JpaRepository<EmployeeSocial, EmployeeSocial.SocialKey> {

    @Query("select socialKey.employeeId from EmployeeSocial where socialKey.providerId = :providerId and socialKey.openId = :openId")
    List<Long> getEmployeeIdByProviderIdAndOpenId(String providerId, String openId);
}
